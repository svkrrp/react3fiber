import './App.css';
import { Canvas } from '@react-three/fiber'
import { OrbitControls } from '@react-three/drei';
import { Suspense } from 'react';
import { Model  } from './Machine1';

function App() {
  return (
    <Canvas>
      <OrbitControls />
      <ambientLight intensity={0.6} />
      <directionalLight intensity={0.5} />
      <Suspense fallback={null}>
        <Model />
      </Suspense>
    </Canvas>
  );
}

export default App;
